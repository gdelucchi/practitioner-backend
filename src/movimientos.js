var express = require('express');
var router = express.Router();
var querys = require('./querys');
let varycons = require('./varycons');

var apiKeyMLab=varycons.apiKeyMLab;
const URI = varycons.URI;
var requestJSON = require('request-json');
var baseMLabURL=varycons.baseMLabURL;
var httpClient = requestJSON.createClient(baseMLabURL);

var mail=require('./enviomail');

router.get('/usuarios/:id/cuentas/:cuenta/movimientos',
function(req, res){
  console.log("GET movimientos");
  let id = req.params.id;
  let cuenta = req.params.cuenta;
  //obtengo los movimientos de la cuenta
  var url=querys.qryObtenerMovimientosMlabPorIdyCuenta(id,cuenta);
  var movimientos;
  console.log("url" + url);
  //Obtengo los datos del usuario origen
  httpClient.get(url,
    function(err, respuestaMLab, body) {
      //console.log("luego de obtener los usuarios de mlab");
      if (!err){
        var respuesta=body[0];
        //recorrer el array de cuentas y quedarse con la cuenta = cuentaOrigen, luego de obtenido obtener el saldo.
        var encontre=false;
        //var usuarioOrigen = respuesta.email;
        let cuentasMlab=respuesta.cuentas;
        var indice=0;
        for (var indice ; indice<cuentasMlab.length && !encontre; indice++){
          if (cuentasMlab[indice].numero == cuenta){
            movimientos=cuentasMlab[indice].movimientos;
            encontre=true;
            indice=indice-1;
          }
        }
        if (encontre){
          //res.send({"msj" : "Devuelvo los movimientos"})
          res.send(movimientos);
        }
        else {
          res.send({"msj" : "No hay movimientos"})
        }
      }
      else {
        console.log("Error conexion con Mlab3");
        res.status(500).end("No es posible conectarme con mlab");
      }
    });
});


// POST movimiento , dado el ID de un usuario y una cuenta se realiza un movimiento
router.post('/usuarios/:id/cuentas/:cuenta/movimientos',
function(req, res){
  console.log("POST transferencia entre de cuentas para usuario");
  let id = req.params.id;
  let cuenta = req.params.cuenta;
  //obtengo el saldo de la cuenta de la cual hago el debito.
  var url=querys.qryObtenerUsuarioMlabPorId(id);
  console.log("muestro url botenerUsuarioMlabPorId: " + url);
  //Obtengo los datos del usuario origen
  httpClient.get(url,
    function(err, respuestaMLab, body) {
      //console.log("luego de obtener los usuarios de mlab");
      if (!err){
        //compruebo que el saldo de la cuenta origen es suficiente.
        let usuarioDestino=req.body.usuariodestino;
        let cuentaDestino= req.body.cuentadestino;
        let latitud= req.body.latitud;
        let longitud= req.body.longitud;
        let monto = req.body.monto;
        console.log("Muestro los datos de entrada: " + usuarioDestino + cuentaDestino + monto + latitud + longitud);
        //obtengo el saldo de la cuenta origen
        let saldo=0;
        var respuesta=body[0];
        var objOrigen=body[0];
        //recorrer el array de cuentas y quedarse con la cuenta = cuentaOrigen, luego de obtenido obtener el saldo.
        var encontre=false;
        var usuarioOrigen = respuesta.email;
        let cuentasMlab=respuesta.cuentas;
        var indice=0;
        for (var indice ; indice<cuentasMlab.length && !encontre; indice++){
          if (cuentasMlab[indice].numero == cuenta){
            saldo=cuentasMlab[indice].saldo;
            encontre=true;
            indice=indice-1;
          }
        }
        if (encontre){
          //Existe la cuenta verifico el saldo.
          console.log("saldo:" + saldo + "monto:" + monto );

          if (monto>saldo){
            console.log("saldo insuficiente");
            res.send({"msj" : "Saldo insuficiente"})
          }
          else {
            //console.log("realizo el movimiento");
            //bajo el saldo y lo actualizao en BD.
            saldo=saldo-monto;
            //actualizo el saldo de la cuenta
            cuentasMlab[indice].saldo=saldo;
            //Me traigo la cuenta del usuarioDestino para hacerle el credito.
            url=querys.qryObtenerUsuarioMlabPorEmail(usuarioDestino);
            console.log("muestro url en movimientos:" + url);
            httpClient.get(url,
              function(errD, respuestaMLabD, bodyD) {
                if (!errD){
                  //Verifico si al menos hay una cuenta para ese usuario
                  if (bodyD.length>0){
                    var objdestino = bodyD[0];
                    var indiceCuentaDestino=0;
                    //Encontre una cuenta para hacer el credito, esta todo pronto para hacer la transferencia
                    //console.log("cuenta destino: " + cuentaDestino);
                    if (cuentaDestino==undefined || cuentaDestino==""){
                      //Se setea la cuenta por defecto la 0 , se podrían hacer más logica complicada en este luegar como rechazar la operacion.
                      indiceCuentaDestino=0;
                      cuentaDestino=objdestino.cuentas[0].numero;
                    }
                    else{
                      var encontreCuentaDestino=false;
                      for (var i=0;i<objdestino.cuentas.length;i++){
                        //console.log("dentro del for: " + objdestino.cuentas[i].numero);
                        if (objdestino.cuentas[i].numero==cuentaDestino){
                          indiceCuentaDestino=i;
                          encontreCuentaDestino=true;
                        }
                      }
                    }
                    //Debo hacer el post a la BD. con el objeto con los datos cambiados del origen.
                    let tamanio=respuesta.cuentas[indice].movimientos.length;
                    let movimiento={
                      "fecha": new Date(),
                      "cuentaorigen":cuenta,
                      "cuentadestino":cuentaDestino,
                      "monto":monto,
                      "latitud" : latitud,
                      "longitud" : longitud
                    };
                    respuesta.cuentas[indice].movimientos[tamanio]=movimiento;
                    console.log("antes de hacer el put: " + url);
                    url=querys.qryObtenerUsuarioMlabPorEmail(usuarioOrigen);
                    httpClient.put(url,respuesta,
                      function(err, respuestaMLab, body){
                        if (!err){
                          //Hago el put con el monto nuevo en el usuario/cuenta destino
                          objdestino.cuentas[indiceCuentaDestino].saldo=objdestino.cuentas[indiceCuentaDestino].saldo + monto;
                          let tamanio=objdestino.cuentas[indiceCuentaDestino].movimientos.length;
                          let movimiento={
                            "fecha": new Date(),
                            "cuentaorigen":cuenta,
                            "cuentadestino":cuentaDestino,
                            "monto":monto,
                            "latitud" : latitud,
                            "longitud" : longitud
                          };
                          url=querys.qryObtenerUsuarioMlabPorEmail(usuarioDestino);
                          objdestino.cuentas[indiceCuentaDestino].movimientos[tamanio]=movimiento;
                          httpClient.put(url,objdestino,
                            function(err, respuestaMLab, body){
                              if (!err){
                                mail.enviarmail(usuarioOrigen);
                                mail.enviarmail(usuarioDestino);
                                res.send({"msj" : "transferencia realizada correctamente"})
                              }
                              else{
                                console.log("Error conexion con Mlab1");
                                res.status(500).end("No es posible conectarme con mlab");
                              }
                            });
                          }
                          else {
                            console.log("Error conexion con Mlab2");
                            res.status(500).end("No es posible conectarme con mlab");
                          }
                        });
                      }
                      else {
                        res.send({"msj": "Error: no existe usuario o no tiene cuenta para hacer el deposito1"});
                      }
                    }
                    else {
                      res.send({"msj": "Error: no existe usuario o no tiene cuenta para hacer el deposito2"});
                    }
                  });
                }
              }
              else {
                res.send({"msj" : "No existe cuenta origen"})
              }
            }
            else {
              console.log("Error conexion con Mlab3");
              res.status(500).end("No es posible conectarme con mlab");
            }
          });
        });

module.exports = router;
