/* Modulo implementado para dar soporte a los token jwt */

var express = require('express'); //"Importo" la clase express
var app = express();  //Me creo un "objeto" de la clase express
var router = express.Router();
var expressJwt=require('express-jwt');
var jwtClave="clavejwt";
var jsonwebtoken=require('jsonwebtoken');


let varycons = require('./varycons');
var apiKeyMLab=varycons.apiKeyMLab;
const URI = varycons.URI;
var querys = require('./querys');

jwt = module.exports;

/* ----------------------------------------------------------------------------------------------*/
// Se revisa si el token es valido.
// Basado en : https://github.com/auth0/express-jwt
  var isRevokedCallback = function(req, payload, done){
    var email = payload.email;
    var auth = req.headers.authorization;
    var tokenjwtrecibido = auth.replace('Bearer ','');

    console.log("---------------------------estoy en isRevokedCallback-----------------------------------");

    let url=querys.qryObtenerUsuarioMlabPorEmail(email);
    httpClient.get(url,
      function(err, respuestaMLab, body){
        if (!err){
          var resp = body;
          if (body[0].token==tokenjwtrecibido){
            // "Token valido"
            return done (null,false);
          }
          else {
            //"Token revocado"
            return done (null,true);
          }
        }
        else {
          console.log("Problema con conexion con Mlab");
          return done(null,true);
        }
    });
  };


/* Función utilizada para crear el jwt tomando como datos los parametros pasados por parametros */
jwt.generoTokenJWT = function (email){
  //Genero token jwtvar
  tokenData = {
  //se pone el email como data dentro del token se puede poner mas información.
    email: email
    }
  //genero el token jwt.
  return token = jsonwebtoken.sign(tokenData, jwtClave, {
       expiresIn: 60 * 60 * 24 // expires in 24 hours
    })
}

router.use(expressJwt({secret:jwtClave,isRevoked:isRevokedCallback}).unless({path: [URI + '/login', URI + '/usuarios']}));
