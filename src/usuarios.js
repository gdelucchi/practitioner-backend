var express = require('express');
var router = express.Router();
var querys = require('./querys');
let varycons = require('./varycons');

var apiKeyMLab=varycons.apiKeyMLab;
const URI = varycons.URI;
var requestJSON = require('request-json');
var baseMLabURL=varycons.baseMLabURL;
var httpClient = requestJSON.createClient(baseMLabURL);

//Importo libreria para encriptar y desencriptar las contraseñas
var encriptar = require('./encriptacion');
var tokenjwt=require('./tokenjwt');

function existeUsuario (email, existe, noExiste){
  let url=querys.qryObtenerUsuarioMlabPorEmail(email);
  //console.log("url:" + 'usuarios?' + queryString + queryStrField + apiKeyMLab);
  httpClient.get(url,
    function(err, respuestaMLab, body){
      if (!err){
        console.log("error:" + err);
          if (body.length>0) {
            existe();
          }
          else {
            noExiste();
          }
      }
      else {
        res.statusCode(500);
        res.send({"msj":"Error al obtener el usuario"})
      }
    });
}

//POST usuarios con mlab, se agrega un usuario nuevo, verifica que no exista previamente.
router.post('/usuarios' ,
  function(req,res){
    console.log("Post usuarios");
    console.log(req.body);
    var msj = "";
    console.log("email ingresado:" + req.body.email);
    var usuarioNuevo;

    let noExiste = function(){
      //No existe el usuario en Mongo , se agrega a la BD.
      console.log("No existe el usuario");
      //Consulto el id mayor que hay en la tabla para agregar el mayor + 1
      let queryString = 's={"id":-1}&l=1&';
      var queryStrField = 'f={"_id":0}&';
      //var httpClient = requestJSON.createClient(baseMLabURL);
      httpClient.get('usuarios?' + queryString + queryStrField + apiKeyMLab,
        function(err, respuestaMLab, body){
          var idMasGrande = body[0].id;
          console.log("Id obteniendo:" + body[0].id);
          console.log("consulta:" + 'usuarios?' + queryString + queryStrField + apiKeyMLab);
          let passencriptada=encriptar.encrypt(req.body.password);
          //Sobreescribo la password de texto plano a encriptada por seguridad.
          req.body.password=passencriptada;
          usuarioNuevo = req.body;
          usuarioNuevo.id = idMasGrande + 1;
          let token=tokenjwt.generoTokenJWT(req.body.email);
          //se envía a Mlab el insert.
          httpClient.post('usuarios?' + queryString + queryStrField + apiKeyMLab, usuarioNuevo,
            function(err, respuestaMLab, body){
              //Hacer las validaciones de la respuesta.
            });
           console.log("peticion:" + 'usuarios?' + queryString + queryStrField + apiKeyMLab);
           msj = "Se agregó el usuario correctamente";
           res.send({ msj, token, usuarioNuevo});
        });
    }

    let existe = function(){
      console.log("Ya existe el usuario");
      res.send({"msj" : "El usuario ya existe"});

    };

    existeUsuario(req.body.email,existe,noExiste);
});

//Method POST login
router.post('/login',
  function (req, res){
    console.log("POST Login");
    var email = req.body.email;
    var pass = req.body.password;
    var passencriptada = encriptar.encrypt(pass);

    let url=querys.qryObtenerUsuarioMlabPorEmail(email);
    httpClient.get(url ,
    function(error, respuestaMLab , body) {
      if (!error){
        console.log(respuesta);
        if(!error){
          if (body[0] != undefined){
          var respuesta = body[0];
            //Verifico si la password es correcta, en lugar de desencriptar la que está en BD encripto la que pasa el usuario.
            if (respuesta.password == passencriptada) {
              console.log("Usuario y password correcto");

              respuesta.token=tokenjwt.generoTokenJWT(email);
              console.log("token:" + token);

              //se agrega el para atributo logged para insertar en BD para saber que el usuario está logueado.
              respuesta.logged=true;
              var urlpost=querys.qryPutUsuarioMLab(respuesta._id.$oid);
              // PUT /databases/{database}/collections/{collection}/{_id}
              httpClient.put(urlpost, respuesta,
                function(errorP, respuestaMLabP, bodyP) {
                  var resP=JSON.stringify(bodyP);
                  let id=respuesta.id;
                  res.send({id,token});
                });
            }
            else {
              console.log("contraseña incorrecta");
              res.status(401).end("nombre de usuario o contraseña incorrecta")
            }
          }
          else {
            console.log("Email Incorrecto");
            res.status(401).end("nombre de usuario o contraseña incorrecta")
          }
        }
        else {
          console.log("No es posible conectarme con mlab");
          res.status(401).end("No es posible conectarme con mlab");
        }
      }
      else {
        console.log("Error conexion con Mlab");
        res.status(500).end("No es posible conectarme con mlab");
      }
    });
});

//Method POST logout
router.post('/logout',
  function (req, res){
    console.log("POST logout");
    var email = req.body.email;
    let url=querys.qryObtenerUsuarioMlabPorEmail(email);
    httpClient.get(url ,
    function(error, respuestaMLab, body) {
      if (!error){
        var respuesta = body[0];
        console.log(respuesta);
        if(respuesta != undefined){
              console.log("logout Correcto");
              //Le agrego el token al usuario
              respuesta.token="";
              respuesta.logged=false;
              var urlpost = `usuarios/${respuesta._id.$oid}/?&`

              // PUT /databases/{database}/collections/{collection}/{_id}
              httpClient.put(urlpost + apiKeyMLab, respuesta,
                function(errorP, respuestaMLabP, bodyP) {
                  if (!errorP){
                    res.send({"msg": "Logout ok"});
                  }
                  else {
                    console.log("Error conexion con Mlab");
                    res.status(500).end("No es posible conectarme con mlab");
                  }
                });
        } else {
          console.log("Error en logout");
          res.send({"msg": "Error en logout"});
        }
      }
      else {
        console.log("Error conexion con Mlab");
        res.status(500).end("No es posible conectarme con mlab");
      }
    });
});

//GET usuarios con mlab.
router.get('/usuarios',
  function(req, res){
    //console.log("GET users mlab");
    //console.log("Cliente HTTP mLab creado.");
    //var queryString = 'f={"_id":0}&';
    let url=querys.qryTodosUsuariosMlab();
    httpClient.get(url,
      function(err, respuestaMLab, body) {
        if (!err){
          if(body.length > 0) {
            response = body;
          }
          else {
            response = {
              "msg" : "Usuario no encontrado"
            }
            res.status(404);
          }
        }
        else{
            response = {
              "msg" : "No es posible conectarme con mlab"
            }
            res.status(500);
        }
        res.send(response);
      });
});

// PUT para cambio de contraseña del usuario.
router.put('/usuarios/:id/cambiopassword',
  function(req, res){
    //var httpClient = requestJSON.createClient(baseMLabURL);
    console.log("PUT cambio la clave de un usuario logueado");
    let passactual=req.body.passactual;
    let passnueva=req.body.passnueva;
    let url = querys.qryObtenerUsuarioMlabPorId(req.params.id);
    httpClient.get(url,
      function(err, respuestaMLab, body) {
        //Valido que es correcta la clave actual
        if (encriptar.encrypt(passactual) == body[0].password){
          //Cambio la clave en el objeto
          body[0].password=encriptar.encrypt(passnueva);
          //actualizo la clave en mlab.
          httpClient.put(url,body[0],
            function(err, respuestaMLab, body){
              //Hacer las validaciones de la respuesta.
              console.log("Clave cambiada en mlab");
              res.send({"msj":"Cambio de clave realizado correctamente"});
            });
          }
          else {
            res.send({"msj":"ERROR: La clave anterior no es correcta"});
          }
    });
});

module.exports = router;
