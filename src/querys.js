let varycons = require('./varycons');
//var baseMLabURL=varycons.baseMLabURL;
var apiKeyMLab=varycons.apiKeyMLab;

function qryTodosUsuariosMlab(){
  return `usuarios?f={"_id":0}&${apiKeyMLab}`;
}

function qryObtenerUsuarioMlabPorEmail(queryStringEmail){
  return `usuarios?q={"email":"${queryStringEmail}"}&${apiKeyMLab}`;
}

function qryObtenerUsuarioMlabPorId(id){
  return `usuarios?q={"id":${id}}&${apiKeyMLab}`;
}

function qryPutUsuarioMLab(oid){
  return `usuarios/${oid}/?&${apiKeyMLab}`
}

function qryTodosCuentasMlabPorId(id){
  return `usuarios?q={"id":${id}}&${apiKeyMLab}`;
}

function qryObtenerMovimientosMlabPorIdyCuenta(id,cuenta){
  return `usuarios?q={"id":${id}}&${apiKeyMLab}`;
}

exports.qryTodosUsuariosMlab=qryTodosUsuariosMlab;
exports.qryObtenerUsuarioMlabPorEmail=qryObtenerUsuarioMlabPorEmail;
exports.qryObtenerUsuarioMlabPorId=qryObtenerUsuarioMlabPorId;
exports.qryPutUsuarioMLab=qryPutUsuarioMLab;
exports.qryTodosCuentasMlabPorId=qryTodosCuentasMlabPorId;
exports.qryObtenerMovimientosMlabPorIdyCuenta=qryObtenerMovimientosMlabPorIdyCuenta;
