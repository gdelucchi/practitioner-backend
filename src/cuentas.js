var express = require('express');
var router = express.Router();
var querys = require('./querys');
let varycons = require('./varycons');

var apiKeyMLab=varycons.apiKeyMLab;
const URI = varycons.URI;
var requestJSON = require('request-json');
var baseMLabURL=varycons.baseMLabURL;
var httpClient = requestJSON.createClient(baseMLabURL);

// GET cuentas , dado el ID de un usuario retorna las cuentas que tiene.
router.get('/usuarios/:id/cuentas',
  function(req, res){
    console.log("GET de cuentas para usuario");
    let id = req.params.id;
    var url = querys.qryTodosCuentasMlabPorId(id);
    //console.log("link get cuentas:" + 'usuarios?' + queryStringId + queryString + apiKeyMLab);
    httpClient.get(url,
      function(err, respuestaMLab, body) {
        if (!err){
          var msj={"msg":"se obtienen las cuentas de usuario"};
          res.send(body);
        }
        else {
          console.log("Error conexion con Mlab");
          res.status(500).end("No es posible conectarme con mlab");
        }
    });
});

module.exports = router;
