var express = require('express'); //"Importo" la clase express
var bodyParser = require('body-parser'); //Libreria para el parseo de los datos del body.
var requestJSON = require('request-json');
var app = express();  //Me creo un "objeto" de la clase express
app.use(bodyParser.json());
var expressJwt=require('express-jwt');
var jwt=require('jsonwebtoken');

//Importo libreria para encriptar y desencriptar las contraseñas
var encriptar = require('./src/encriptacion');
var jwt1 = require('./src/tokenjwt');

//Importo libreria de querys a BD.
var querys = require('./src/querys');

//Obtengo las constantes y variables del archivo importado.
let varycons = require('./src/varycons');
var baseMLabURL=varycons.baseMLabURL;
var apiKeyMLab=varycons.apiKeyMLab;
const URI = varycons.URI;
var port = varycons.port;

//Algunas otras variables
var jwtClave="clavejwt";
app.use(express.static('publica'));
var httpClient = requestJSON.createClient(baseMLabURL);

/* ----------------------------------------------------------------------------------------------*/
// Se revisa si el token es valido.
// Basado en : https://github.com/auth0/express-jwt
var isRevokedCallback = function(req, payload, done){
  var email = payload.email;
  var auth = req.headers.authorization;
  var tokenjwtrecibido = auth.replace('Bearer ','');

  console.log("---------------------------estoy en isRevokedCallback-----------------------------------");

  let url=querys.qryObtenerUsuarioMlabPorEmail(email);
  httpClient.get(url,
    function(err, respuestaMLab, body){
      if (!err){
        var resp = body;
        if (body[0].token==tokenjwtrecibido){
          // "Token valido"
          return done (null,false);
        }
        else {
          //"Token revocado"
          return done (null,true);
        }
      }
      else {
        console.log("Problema con conexion con Mlab");
        return done(null,true);
      }
  });
};

app.use(expressJwt({secret:jwtClave,isRevoked:isRevokedCallback}).unless({path: [URI + '/login', URI + '/usuarios']}));

app.use(URI , require('./src/movimientos'))
app.use(URI , require('./src/usuarios'));
app.use(URI , require('./src/cuentas'));

//para que funcione el cors
app.use(function(req, res, next) {
 res.header("Access-Control-Allow-Origin", "*");
 res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
 next();
});

//Levanto el servidor en el puerto del host.
app.listen(port);

console.log('Escuchando puerto: ' + port);
