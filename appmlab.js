var express = require('express'); //"Importo" la clase express
var bodyParser = require('body-parser'); //Libreria para el parseo de los datos del body.
var requestJSON = require('request-json');
var usersfile = require ('./users.json');
var accountsfile = require ('./accounts.json');
var app = express();  //Me creo un "objeto" de la clase express
app.use(bodyParser.json());
var port = process.env.PORT || 3000; //Se fija si existe la variable de entorno en el Sistema Operativo , si existe la usa sino asigna el 3000
const URI = '/api-uruguay/v1/';

var baseMLabURL = 'https://api.mlab.com/api/1/databases/techbduruguay/collections/';
var apiKeyMLab =  'apiKey=t59osqPf7muy-mgt1yAmqylZXOO_04wh';
var clienteMlab;

//GET usuarios con mlab.
app.get(URI + 'users',
  function(req, res){
    console.log("GET users mlab");
    var httpClient = requestJSON.createClient(baseMLabURL);
    console.log("Cliente HTTP mLab creado.");
    var queryString = 'f={"_id":0}&';
    httpClient.get('users?' + queryString + apiKeyMLab,
      function(err, respuestaMLab, body) {
        var response = {};
        if(err) {
            response = {
              "msg" : "Error obteniendo usuario."
            }
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {
              "msg" : "Usuario no encontrado."
            }
            res.status(404);
          }
        }
        res.send(response);
      });
});

//GET usuarios con ID en mlab
app.get(URI + 'user/:id',
  function(req, res){
    console.log("GET con parametros Mlab");
    //console.log(req.params.id);
    var id = req.params.id;
    var queryString = 'q={"id":' + id + '}&';
    var queryStrField = 'f={"_id":0}&';
    var httpClient = requestJSON.createClient(baseMLabURL);
    httpClient.get('users?' + queryString + queryStrField + apiKeyMLab,
      function(err, respuestaMLab, body){
        console.log("Respuesta mLab correcta.");
      //  var respuesta = body[0];
        var response = {};
        if(err) {
            response = {
              "msg" : "Error obteniendo usuario."
            }
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {
              "msg" : "Usuario no encontrado."
            }
            res.status(404);
          }
        }
        res.send(response);
      });
  });

//GET con query string
app.get(URI + "user?",
  function (req, res){
    console.log("GET con query string");
    let name = req.query.qname;
    let lastName = req.query.qlast_name;
    //console.log("Se leen los paramtros" + name + lastName);
    for (var user in usersfile){
      //console.log("Leo los datos del archivo" + user.first_name + user.last_name + user);
      if (usersfile[user].first_name == name && usersfile[user].last_name == lastName){
        msj = {"mensaje":"Usuario encontrado"};
        break;
      }
      else {
        msj = {"mensaje":"Usuario NO encontrado"};
      }
    }
    res.send ({msj});
  });

//POST users con mlab
app.post(URI + 'users' ,
  function(req,res){
    console.log("Post users");
    console.log(req.body);
    var msj = "";
    //Consulto el id mayor que hay en la tabla para agregar el mayor + 1
    let queryString = 's={"id":-1}&l=1&'
    var queryStrField = 'f={"_id":0}&';
    var httpClient = requestJSON.createClient(baseMLabURL);
    httpClient.get('users?' + queryString + queryStrField + apiKeyMLab,
      function(err, respuestaMLab, body){
        var idMasGrande = body[0].id;
        console.log("Id obteniendo:" + body[0].id);
        console.log("consulta:" + 'users?' + queryString + queryStrField + apiKeyMLab);
        var usuarioNuevo = req.body;
        usuarioNuevo.id = idMasGrande + 1;
        //se envía a Mlab el insert.
        httpClient.post('users?' + queryString + queryStrField + apiKeyMLab, usuarioNuevo,
          function(err, respuestaMLab, body){
            //Hacer las validaciones de la respuesta.
          });
         console.log("peticion:" + 'users?' + queryString + queryStrField + apiKeyMLab);
         msj = "Se agregó el usuario correctamente"
         res.send({ msj, usuarioNuevo});
      });
});

//PUT users
app.put(URI + 'users/:id' ,
  function(req,res){
    console.log('PUT users');
    let msjResponse;
    let statusCode;
    let idUpdate = req.params.id;
    if (usersfile[idUpdate-1] != null) {
      //res.send({'mensaje': 'está el registro ok'});
      msjResponse = 'Update OK';
      statusCode = 200;
      let userNuevo = req.body;
      userNuevo.id = idUpdate ;
      usersfile[idUpdate-1]=userNuevo;

    }
    else {
      msjResponse = 'Update failed';
      statusCode = 400;
    }
    res.send(msjResponse).statusCode(statusCode);
  });

//DELETE users
app.delete(URI + 'users/:id' ,
  function(req,res){
    let msjResponse;
    let statusCode;
    let idUpdate = req.params.id;
    if (usersfile[idUpdate-1] != null) {
      //Existe un registro a borrar
      msjResponse = 'Se borro el registro correctamente';
      statusCode = 200;
      usersfile.splice(idUpdate -1 ,1); //Se le pasan de parametros a partir de donde borrar y cuantos registros borrar
    }
    else {
      //No existe registro a borrar
      msjResponse = 'No hay registro a borrar';
      statusCode = 400;
    }
    res.send(msjResponse).statusCode(statusCode);

  });


  //PUT of user
  app.put(URI + 'users/:id',
  function(req, res) {
    var id = req.params.id;
    var queryStringID = 'q={"id":' + id + '}&';
    var clienteMlab = requestJSON.createClient(baseMLabURL);
    clienteMlab.get('user?'+ queryStringID + apikeyMLab ,
      function(error, respuestaMLab , body) {
       var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
       clienteMlab.put(baseMLabURL + 'user?q={"id": ' + id + '}&' + apikeyMLab, JSON.parse(cambio),
        function(error, respuestaMLab, body) {
          console.log("body:"+ body);
         res.send(body);
        });
   });
  });

  //DELETE user with id
  app.delete(URI + "users/:id",
    function(req, res){
      console.log("entra al DELETE");
      console.log("request.params.id: " + req.params.id);
      var id = req.params.id;
      var queryStringID = 'q={"id":' + id + '}&';
      console.log(baseMLabURL + 'user?' + queryStringID + apikeyMLab);
      var httpClient = requestJSON.createClient(baseMLabURL);
      httpClient.get('user?' +  queryStringID + apikeyMLab,
        function(error, respuestaMLab, body){
          var respuesta = body[0];
          console.log("body delete:"+ respuesta);
          httpClient.delete(baseMLabURL + "user/" + respuesta._id.$oid +'?'+ apikeyMLab,
            function(error, respuestaMLab,body){
              res.send(body);
          });
        });
    });

  //Method POST login
  app.post(URI + "login",
    function (req, res){
      console.log("POST /colapi/v3/login");
      var email = req.body.email;
      var pass = req.body.password;
      var queryStringEmail = 'q={"email":"' + email + '"}&';
      var queryStringpass = 'q={"password":' + pass + '}&';
      var  clienteMlab = requestJSON.createClient(baseMLabURL);
      clienteMlab.get('user?'+ queryStringEmail + apikeyMLab ,
      function(error, respuestaMLab , body) {
        console.log("entro al body:" + body );
        var respuesta = body[0];
        console.log(respuesta);
        if(respuesta != undefined){
            if (respuesta.password == pass) {
              console.log("Login Correcto");
              var session = {"logged":true};
              var login = '{"$set":' + JSON.stringify(session) + '}';
              console.log(baseMLabURL+'?q={"id": ' + respuesta.id + '}&' + apikeyMLab);
              clienteMlab.put('user?q={"id": ' + respuesta.id + '}&' + apikeyMLab, JSON.parse(login),
                function(errorP, respuestaMLabP, bodyP) {
                  res.send(body[0]);
                });
            }
            else {
              res.send({"msg":"contraseña incorrecta"});
            }
        } else {
          console.log("Email Incorrecto");
          res.send({"msg": "email Incorrecto"});
        }
      });
  });


  //Method POST logout
  app.post(URI + "logout",
    function (req, res){
      console.log("POST /colapi/v3/logout");
      var email = req.body.email;
      var queryStringEmail = 'q={"email":"' + email + '"}&';
      var  clienteMlab = requestJSON.createClient(baseMLabURL);
      clienteMlab.get('user?'+ queryStringEmail + apikeyMLab ,
      function(error, respuestaMLab, body) {
        console.log("entro al get");
        var respuesta = body[0];
        console.log(respuesta);
        if(respuesta != undefined){
              console.log("logout Correcto");
              var session = {"logged":true};
              var logout = '{"$unset":' + JSON.stringify(session) + '}';
              console.log(logout);
              clienteMlab.put('user?q={"id": ' + respuesta.id + '}&' + apikeyMLab, JSON.parse(logout),
                function(errorP, respuestaMLabP, bodyP) {
                  res.send(body[0]);
                //res.send({"msg": "logout Exitoso"});
                });
        } else {
          console.log("Error en logout");
          res.send({"msg": "Error en logout"});
        }
      });
  });

//cuentas
app.get(URI + 'accounts',
  function(req, res){
    res.send (accountsfile);
    console.log('GET accounts');
});

//Levanto el servidor en el puerto 3000
app.listen(port);

console.log('Escuchando puerto: ' + port);
