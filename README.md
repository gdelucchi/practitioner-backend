# practitioner-backend

Proyecto Backend para el curso de Practitioner
octubre 2018

Aplicación Backend realizada en NodeJS creada el curso de Practitioner. En la misma se puede hacer login y logout de un usuario, obtener sus cuentas y sus movimientos, también se puede realizar una transferencia entre distintos usuarios. Se puede acceder a los request hechos por Postman en el siguiente link: https://www.getpostman.com/collections/fb3edecaf9e4eb3ef6c2 Tener en cuenta que se debe generar primero un jwt realizando un login de usuario y luego el jwt devuelto es utilizado en el resto de los request a los demás endpoints.